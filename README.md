# Simple Application For React #

### How do I get set up? ###

* Clone the repository

    `https://bitbucket.org/flashcorp/react-simple-test`

    `cd react-simple-test/`

* Install Dependencies

    `npm install`

* Run using webpack

    `npm run start`

* Done

  `Browse http://localhost:5000/public`

### Extras ###

* Build

    `npm run build`

### Tools ###

* NodeJS
* ReactJS
* React Router
* Bootstrap
* Webpack

### Contact ###

Creator: Ej Corpuz
Email: ejan.corp@gmail.com
