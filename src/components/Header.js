import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <div className="page-header">
                <div className="row">
                    <div className="col-lg-12">
                        <h1>React</h1>
                        <p className="lead">Testing Capabilities</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;
