import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Menu extends Component {
  render() {
      return (
            <ul className="nav nav-pills">
                <li className="nav-item">
                    <Link to='/' className="nav-link">Home</Link>
                </li>
                <li className="nav-item">
                    <Link to='signup' className="nav-link">Signup</Link>
                </li>
                <li className="nav-item">
                    <Link to='forms' className="nav-link">Forms</Link>
                </li>
            </ul>
      );
  }
}

export default Menu;
