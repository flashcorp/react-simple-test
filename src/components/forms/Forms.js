import React, { Component } from 'react';
import ToggleDisplay from 'react-toggle-display'
import _ from 'lodash';

class Forms extends Component {
    constructor() {
        super();
        this.state = {
            selectType: 0,
            selectSize: 0,
            selectSide: [],
            result: null
        }
    }
    showResult(event) {
        const result = `Type: ${this.state.selectType}, Size: ${this.state.selectSize}, Side: ${this.state.selectSide}`;
        this.setState({ result: result });
    }
    onChangeType(event) {
        this.setState({ selectType: parseInt(event.target.value) })
        if (!this.state.selectType) {
            this.setState({ selectSize: 0, selectSide: [], result: null });
        }
    }
    onChangeSize(event) {
        this.setState({ selectSize: parseInt(event.target.value) });
    }
    onChangeSide(event) {
        const side = _.clone(this.state.selectSide);
        if (event.target.checked) {
            side.push(event.target.value);
            this.setState({ selectSide: side});
            return;
        }
        this.setState({ selectSide: side.filter(val => val !== event.target.value) })
    }
    render() {
        return (
            <div className="row">
                <h5>Forms</h5>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4">
                            <div className="form-group">
                                <label htmlFor="type">Select Type</label>
                                <select onChange={ this.onChangeType.bind(this) } value={ this.state.selectType } className="form-control" id="type">
                                    <option value="0">Select Type</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <ToggleDisplay if={Boolean(this.state.selectType)}>
                        <div className="row">
                            <div className="col-lg-4">
                                <h6>Size</h6>
                                <div className="form-check">
                                    <label className="form-check-label">
                                        <input onChange={ this.onChangeSize.bind(this) } type="radio" className="form-check-input" name="optionsRadios" value="50" />
                                        50
                                    </label>
                                </div>
                                <div className="form-check">
                                    <label className="form-check-label">
                                        <input onChange={ this.onChangeSize.bind(this) } type="radio" className="form-check-input" name="optionsRadios" value="100" />
                                        100
                                    </label>
                                </div>
                            </div>
                        </div>
                    </ToggleDisplay>
                    <ToggleDisplay if={Boolean(this.state.selectSize) && Boolean(this.state.selectType)}>
                        <div className="row">
                            <div className="col-lg-4">
                                <h6>Side Fruits</h6>
                                <div className="form-check">
                                    <label className="form-check-label">
                                        <input onChange={ this.onChangeSide.bind(this) } type="checkbox" value="Tomato" className="form-check-input" />
                                        Tomato
                                    </label>
                                </div>
                                <div className="form-check">
                                    <label className="form-check-label">
                                        <input onChange={ this.onChangeSide.bind(this) } type="checkbox" value="Banana" className="form-check-input" />
                                        Banana
                                    </label>
                                </div>
                                <div className="form-check">
                                    <label className="form-check-label">
                                        <input onChange={ this.onChangeSide.bind(this) } type="checkbox" value="Mango" className="form-check-input" />
                                        Mango
                                    </label>
                                </div>
                            </div>
                        </div>
                    </ToggleDisplay>
                    <ToggleDisplay if={Boolean(this.state.selectSide)}>
                        <button onClick={ this.showResult.bind(this) } type="button" className="btn btn-success">Submit</button>
                    </ToggleDisplay>
                    <ToggleDisplay if={Boolean(this.state.result)}>
                        <hr/>
                        <div className="alert alert-dismissible alert-info">
                            { this.state.result }
                        </div>
                    </ToggleDisplay>
                </div>
            </div>
        );
    }
}

export default Forms;
