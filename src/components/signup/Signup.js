import React, { Component } from 'react';

import Storage from './Storage';
import Wizard from './Wizard';
import Profile from './steps/Profile';
import Account from './steps/Account';
import Success from './steps/Success';

let UserInformation = {
    firstName: null,
    lastName: null,
    email: null,
    password: null,
};

class Signup extends Component {
    constructor(props) {
        super(props);
        this.storage = new Storage();
        this.state = {
            step: 1
        }
    }
    componentDidMount () {

        this.setState({ step: this.storage.getStep() || 1 });
        this.save(this.storage.getData());
    }
    next() {
        this.setState({ step: this.state.step + 1 });
        this.storage.setStep(this.state.step + 1);
    }
    prev() {
        this.setState({ step : this.state.step - 1 });
        this.storage.setStep(this.state.step - 1);
    }
    save(field) {
        UserInformation = Object.assign({}, UserInformation, field)
        this.storage.setData(UserInformation);
    }
    reset() {
        for(let key in UserInformation) {
            UserInformation[key] = null;
        }
        this.setState({ step : 1 });
        this.storage.clearAll();
    }
    getCurrentStep() {
        switch (this.state.step) {
            case 2:
                return <Account
                        defaults={ UserInformation }
                        save={ this.save.bind(this) }
                        next={ this.next.bind(this) }
                        prev={ this.prev.bind(this) }
                        />;
                break;
            case 3:
                return <Success
                        reset={ this.reset.bind(this) }
                        />;
                break;
            default:
                return <Profile
                        defaults={ UserInformation }
                        save={ this.save.bind(this) }
                        next={ this.next.bind(this) }
                        />;
        }
    }
    render() {
        return (
            <div className="row">
                <h5>Signup</h5>
                <div className="container">
                    <Wizard step={ this.state.step } />
                    { this.getCurrentStep() }
                </div>
            </div>
        );
    }
}

export default Signup;
