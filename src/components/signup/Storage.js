class Storage {
    constructor() {
        this.stepKey = 'signup-key';
        this.dataKey = 'signup-data';
    }
    loadAll() {
        return {
            step: this.getStep(),
            data: this.getData(),
        }
    }
    clearAll() {
        localStorage.removeItem(this.stepKey);
        localStorage.removeItem(this.dataKey);
    }
    setStep(step = 1) {
        localStorage.setItem(this.stepKey, parseInt(step));
    }
    getStep() {
        return parseInt(localStorage.getItem(this.stepKey));
    }
    setData(data = {}) {
        localStorage.setItem(this.dataKey, JSON.stringify(data));
    }
    getData() {
        return JSON.parse(localStorage.getItem(this.dataKey));
    }
}

export default Storage;
