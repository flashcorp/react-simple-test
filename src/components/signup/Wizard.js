import React, { Component } from 'react';

class Wizard extends Component {
    render() {
        return (
            <div className="col-lg-2">
                <ul className="list-group">
                    <li className={'list-group-item ' + (this.props.step == 1 ? 'active' : '')}>
                        <span className="badge badge-pill badge-primary float-xs-right">1</span>
                        Profile Details
                    </li>
                    <li className={'list-group-item ' + (this.props.step == 2 ? 'active' : '')}>
                        <span className="badge badge-pill badge-primary float-xs-right">2</span>
                        Account Details
                    </li>
                </ul>
            </div>
        )
    }
}

export default Wizard;
