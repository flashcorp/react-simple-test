import React, { Component } from 'react';

class Account extends Component {
    save(event) {
        event.preventDefault();

        const fields = {
            email: this.refs.email.value,
            password: this.refs.password.value,
        };

        this.props.save(fields);
        this.props.next();
    }
    back() {
        this.props.prev();
    }
    render() {
        return (
            <div className="col-lg-6">
                <h5>Account Details</h5>

                <div className="row">
                    <div className="col-lg-12">
                        <form>
                            <div className="form-group">
                                <label htmlFor="inputEmail">Email Address:</label>
                                <input
                                    defaultValue={ this.props.defaults.email }
                                    ref="email"
                                    type="email"
                                    className="form-control"
                                    id="inputEmail"
                                    placeholder="example@gmail.com"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="inputPassword">Password:</label>
                                <input
                                    defaultValue={ this.props.defaults.password }
                                    ref="password"
                                    type="password"
                                    className="form-control"
                                    id="inputPassword"
                                    placeholder=""
                                />
                            </div>
                            <button onClick={ this.back.bind(this) } type="button" className="btn btn-primary pull-left">Previous</button>
                            <button onClick={ this.save.bind(this) } type="button" className="btn btn-primary pull-right">Next</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Account;
