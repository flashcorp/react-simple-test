import React, { Component } from 'react';

class Profile extends Component {
    save(event) {
        event.preventDefault();

        const fields = {
            firstName: this.refs.firstName.value,
            lastName: this.refs.lastName.value,
        };

        this.props.save(fields);
        this.props.next();
    }
    render() {
        return (
            <div className="col-lg-6">
                <h5>Profile Details</h5>

                <div className="row">
                    <div className="col-lg-12">
                        <form>
                            <div className="form-group">
                                <label htmlFor="inputFirstName">First Name</label>
                                <input
                                    defaultValue={ this.props.defaults.firstName }
                                    ref="firstName"
                                    type="text"
                                    className="form-control"
                                    id="inputFirstName"
                                    placeholder="First Name"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="inputLastName">Last Name</label>
                                <input
                                    defaultValue={ this.props.defaults.lastName }
                                    ref="lastName"
                                    type="text"
                                    className="form-control"
                                    id="inputLastName"
                                    placeholder="Last Name"
                                />
                            </div>
                            <button onClick={ this.save.bind(this) } type="button" className="btn btn-primary pull-right">Next</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Profile;
