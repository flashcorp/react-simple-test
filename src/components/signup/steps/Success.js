import React, { Component } from 'react';

class Success extends Component {
    reset() {
        this.props.reset();
    }
    render() {
        return (
            <div className="col-lg-6">
                <h4>Successfully Signed Up!</h4>
                <button onClick={ this.reset.bind(this) } type="button" className="btn btn-success">Done!</button>
            </div>
        )
    }
}

export default Success;
