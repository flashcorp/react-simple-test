import React from 'react';
import ReactDom from 'react-dom';
import { Route, HashRouter } from 'react-router-dom';

import routes from './routes';

ReactDom.render(
    <HashRouter children={routes} />,
    document.getElementById('app')
);
