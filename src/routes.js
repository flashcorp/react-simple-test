import React from 'react';
import { Route } from 'react-router-dom';

import App from './components/App';
import Signup from './components/signup/Signup';
import Forms from './components/forms/Forms';

export default (
    <div className="row">
        <div className="container">
            <Route component={App} />
        </div>
        <div className="container">
            <Route component={Signup} path='/signup' />
            <Route component={Forms} path='/forms' />
        </div>
    </div>
);
