/* eslint-disable */

var path = require('path');
var webpack = require('webpack');

var DIST_DIR = path.resolve(__dirname, 'public');
var SRC_DIR = path.resolve(__dirname, 'src');

module.exports = {
    entry: [
        'webpack-dev-server/client?http://localhost:5000',
        'webpack/hot/dev-server',
        SRC_DIR + '/index'
    ],
    devServer: {
        historyApiFallback: true,
    },
    output: {
        path: DIST_DIR + '/dist',
        filename: 'bundle.js',
        publicPath: '/public/dist'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ],
    module: {
        loaders: [{
            test: /\.js?/,
            include: SRC_DIR,
            loader: 'babel-loader',
            query: {
                presets: ['react', 'env', 'stage-2']
            }
        }]
    }
};
